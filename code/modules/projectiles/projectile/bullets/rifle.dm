//7.62x51 NATO / .308 Winchester
/obj/item/projectile/bullet/a762
	damage = 40
	armour_penetration = 20

/obj/item/projectile/bullet/a762/ap
	damage = 35
	armour_penetration = 40

/obj/item/projectile/bullet/a762/jhp
	damage = 45
	armour_penetration = 0

/obj/item/projectile/bullet/a762/sport //.308 Winchester
	damage = 40
	armour_penetration = 30

//5.56x45mm
/obj/item/projectile/bullet/a556
	damage = 35
	armour_penetration = 10

/obj/item/projectile/bullet/a556/ap
	damage = 30
	armour_penetration = 30

/obj/item/projectile/bullet/a556/jhp
	damage = 40
	armour_penetration = -10

/obj/item/projectile/bullet/a556/sport
	damage = 35
	armour_penetration = 20

//2mm Electromagnetic
/obj/item/projectile/bullet/c2mm
	damage = 60
	armour_penetration = 40
